package test2;

public interface ThreeDimensionalShapeInterface {
	public double calculateVolume();
	public double printDetails();
}
